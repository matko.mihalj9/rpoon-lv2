using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON-lv2
{
    class DiceRoller : ILogable
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;
        //private ILogger logger;

        public DiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
            //this.SetLogger(new ConsoleLogger());
        }
        public void InsertDie(Die die)
        {
            dice.Add(die);
        }
        //public void SetLogger(ILogger logger)
        //{
        //    this.logger = logger;
        //}

        
        //View of the results
        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(
           this.resultForEachRoll
           );
        }

         public string GetStringRepresentation()
        {
            StringBuilder stringbuilder = new StringBuilder();
            foreach(int result in resultForEachRoll)
            {
                stringbuilder.Append(result.ToString()).Append("\n");
            }
            return stringbuilder.ToString();
        }

         public void RollAllDice()
            {
            //clear results of previous rolling
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }

        public List<int> ResultForEachRoll
        {
            get { return resultForEachRoll; }
        }
        public int DiceCount
        {
            get { return dice.Count; }
        }
        //public void LogRollingResults()
        //{
        //    foreach(int result in resultForEachRoll)
        //    {
        //        logger.Log(result.ToString());
        //    }
        //}
       
    } }
