using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON-lv2
{
    class FileLogger:ILogger
    {
        private string filePath;


        //public void Log(string message)
        //{
        //    using (System.IO.StreamWriter writer =
        //            new System.IO.StreamWriter(this.filePath))
        //    {
        //        writer.WriteLine(message);
        //    }
        //} 4. zadatak
        
        public FileLogger(string path)
        {
            this.filePath = path;
        } 

        public void Log(ILogable data)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath))
            {
                writer.WriteLine(data.GetStringRepresentation());
            }
        }
        
    }
}
