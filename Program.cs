using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON-lv2
{
    class Program
    {
        static void Main(string[] args)
        {
            DiceRoller diceRoller = new DiceRoller();
            Random randgen=new Random();
            int i = 0;
            for (i = 0; i < 20; i++)
            {
                diceRoller.InsertDie(new Die(6,randgen));
            }
            diceRoller.RollAllDice();
            diceRoller.GetRollingResults();

            foreach (int result in diceRoller.ResultForEachRoll)
            {
                Console.WriteLine(result);
               
            }
            Console.ReadKey();

        }
    }
}
