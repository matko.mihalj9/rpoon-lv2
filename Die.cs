using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON-lv2
{
    class Die
    {
        private int numberOfSides;
        //private Random randomGenerator;
        private RandomGenerator randomGenerator;
        public Die(int numberOfSides, Random random)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = RandomGenerator.GetInstance();
        }
        public int Roll()
        {
            int rolledNumber = randomGenerator.NextInt(1, numberOfSides + 1);
            return rolledNumber;
        }
        public int getNumberofSides()
        {
            return numberOfSides;
        }

    }
}
